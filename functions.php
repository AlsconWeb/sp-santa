<?php
/**
 * Functions file.
 *
 * @package iwpdev/sp-santa
 */

use Iwpdev\SpSanta\Main;

require_once __DIR__ . '/vendor/autoload.php';

new Main();
