<?php
/**
 * Header theme template.
 *
 * @package iwpdev/sp-santa
 */

$body_class = is_front_page() || is_page( 13 ) ? 'home' : '';
?>
<!DOCTYPE html>
<html>
<head> 
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" id="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<title><?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?></title>
	<?php wp_head(); ?>
</head>
<body <?php body_class( $body_class ); ?>>
<header>
	<div class="container">
		<?php the_custom_logo(); ?>
	</div>
</header>
