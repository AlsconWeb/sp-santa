<?php
/**
 * Template Name: Quiz
 *
 * @package iwpdev/sp-santa
 */

get_header();
?>
	<section>
		<div class="container">
			<?php
			if ( have_posts() ) {
				while ( have_posts() ) {
					the_post();
					$quiz = carbon_get_the_post_meta( 'sps_quiz_questions' );
					?>
					<form id="quiz" method="post">
						<div class="start active">
							<?php the_content(); ?>
							<a class="button" href="#">
								<?php echo esc_html( carbon_get_the_post_meta( 'sps_button_text_start_quiz' ) ?? '' ); ?>
							</a>
						</div>
						<?php
						if ( ! empty( $quiz ) ) {
							foreach ( $quiz as $key => $item ) {
								$image     = wp_get_attachment_image_url( $item['image'], 'full' ) ?? 'https://via.placeholder.com/358x358';
								$questions = $item['questions'];
								?>
								<div class="question" id="question-<?php echo esc_attr( $key ); ?>">
									<div class="img">
										<img
												src="<?php echo esc_url( $image ); ?>"
												alt="<?php echo esc_html( get_the_title( $item['image'] ) ?? '' ); ?>">
									</div>
									<div class="description">
										<h3 class="title">
											<?php echo wp_kses_post( wpautop( $item['title'] ) ); ?>
										</h3>
										<?php
										if ( ! empty( $questions ) ) {
											shuffle( $questions );
											foreach ( $questions as $q_key => $question ) {
												?>
												<div class="radio-button">
													<input
															id="<?php echo esc_attr( $key . '-' . $q_key ); ?>"
															type="radio"
															data-type="<?php echo esc_attr( $question['question'] ); ?>"
															name="question-<?php echo esc_attr( $key ); ?>">
													<label for="<?php echo esc_attr( $key . '-' . $q_key ); ?>">
														<?php echo wp_kses_post( wpautop( $question['text'] ) ); ?>
													</label>
												</div>
												<?php
											}
										}
										?>
									</div>
								</div>
								<?php
							}
						}
						?>

						<div
								class="finish elf"
								style="--bg: url('<?php echo esc_url( get_template_directory_uri() . '/assets/img/elf.webp' ); ?>');">
							<p class="button">
								<?php echo esc_html( carbon_get_the_post_meta( 'sps_answer_title_elf' ) ?? '' ); ?>
							</p>
							<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'sps_answer_elf' ) ?? '' ) ); ?>
							<div class="sheared-link">
								<p><?php echo esc_html( carbon_get_the_post_meta( 'sps_button_shear_button' ) ); ?></p>
								<?php echo do_shortcode( '[spbsm-share-buttons]' ); ?>
							</div>
							<div class="links">
								<a class="link icon-back" href="#">
									<?php echo esc_html( carbon_get_the_post_meta( 'sps_button_text_return_quiz' ) ?? '' ); ?>
								</a>
								<a
										class="link icon-earth"
										href="<?php echo esc_url( carbon_get_the_post_meta( 'sps_button_link_to_shop' ) ?? '#' ); ?>">
									<?php echo esc_html( carbon_get_the_post_meta( 'sps_button_text_to_shop' ) ?? '' ); ?>
								</a>
							</div>
						</div>

						<div
								class="finish strategist"
								style="--bg: url('<?php echo esc_url( get_template_directory_uri() . '/assets/img/strateg.webp' ); ?>');">
							<p class="button">
								<?php echo esc_html( carbon_get_the_post_meta( 'sps_answer_title_strategist' ) ?? '' ); ?>
							</p>
							<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'sps_answer_strategist' ) ?? '' ) ); ?>
							<div class="sheared-link">
								<p><?php echo esc_html( carbon_get_the_post_meta( 'sps_button_shear_button' ) ); ?></p>
								<?php echo do_shortcode( '[spbsm-share-buttons]' ); ?>
							</div>
							<div class="links">
								<a class="link icon-back" href="#">
									<?php echo esc_html( carbon_get_the_post_meta( 'sps_button_text_return_quiz' ) ?? '' ); ?>
								</a>
								<a
										class="link icon-earth"
										href="<?php echo esc_url( carbon_get_the_post_meta( 'sps_button_link_to_shop' ) ?? '#' ); ?>">
									<?php echo esc_html( carbon_get_the_post_meta( 'sps_button_text_to_shop' ) ?? '' ); ?>
								</a>
							</div>
						</div>

						<div
								class="finish fireman"
								style="--bg: url('<?php echo esc_url( get_template_directory_uri() . '/assets/img/fireman.webp' ); ?>');">
							<p class="button">
								<?php echo esc_html( carbon_get_the_post_meta( 'sps_answer_title_fireman' ) ?? '' ); ?>
							</p>
							<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'sps_answer_fireman' ) ?? '' ) ); ?>
							<div class="sheared-link">
								<p><?php echo esc_html( carbon_get_the_post_meta( 'sps_button_shear_button' ) ); ?></p>
								<?php echo do_shortcode( '[spbsm-share-buttons]' ); ?>
							</div>
							<div class="links">
								<a class="link icon-back" href="#">
									<?php echo esc_html( carbon_get_the_post_meta( 'sps_button_text_return_quiz' ) ?? '' ); ?>
								</a>
								<a
										class="link icon-earth"
										href="<?php echo esc_url( carbon_get_the_post_meta( 'sps_button_link_to_shop' ) ?? '#' ); ?>">
									<?php echo esc_html( carbon_get_the_post_meta( 'sps_button_text_to_shop' ) ?? '' ); ?>
								</a>
							</div>
						</div>

						<div
								class="finish grinch"
								style="--bg: url('<?php echo esc_url( get_template_directory_uri() . '/assets/img/bg-two.webp' ); ?>');">
							<p class="button">
								<?php echo esc_html( carbon_get_the_post_meta( 'sps_answer_title_grinch' ) ?? '' ); ?>
							</p>
							<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'sps_answer_grinch' ) ?? '' ) ); ?>
							<div class="sheared-link">
								<p><?php echo esc_html( carbon_get_the_post_meta( 'sps_button_shear_button' ) ); ?></p>
								<?php echo do_shortcode( '[spbsm-share-buttons]' ); ?>
							</div>
							<div class="links">
								<a class="link icon-back" href="#">
									<?php echo esc_html( carbon_get_the_post_meta( 'sps_button_text_return_quiz' ) ?? '' ); ?>
								</a>
								<a
										class="link icon-earth"
										href="<?php echo esc_url( carbon_get_the_post_meta( 'sps_button_link_to_shop' ) ?? '#' ); ?>">
									<?php echo esc_html( carbon_get_the_post_meta( 'sps_button_text_to_shop' ) ?? '' ); ?>
								</a>
							</div>
						</div>
					</form>
					<?php
				}
			}
			?>
		</div>
	</section>
<?php
get_footer();
