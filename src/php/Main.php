<?php
/**
 * Main class theme.
 *
 * @package iwpdev/sp-santa
 */

namespace Iwpdev\SpSanta;

/**
 * Main class file.
 */
class Main {
	/**
	 * Theme version.
	 */
	public const SPS_VERSION = '1.0.0';

	/**
	 * Action and nonce code form ajax.
	 */
	public const SPS_COUNTER_ACTION_NAME = 'sps_counter_ajax';

	/**
	 * Options name.
	 */
	public const SPS_OPTIONS_COUNT_NAME = 'sps_quiz_cont';

	/**
	 * Main construct.
	 */
	public function __construct() {
		$this->init();

		new CarboneFields();
	}

	/**
	 * Init all hooks.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts' ] );
		add_action( 'after_setup_theme', [ $this, 'add_theme_support' ] );
		add_action( 'wp_dashboard_setup', [ $this, 'add_dashboard_widgets' ] );

		add_filter( 'mime_types', [ $this, 'add_support_mimes' ] );
		add_filter( 'get_custom_logo', [ $this, 'change_logo_class' ] );

		add_action( 'wp_ajax_nopriv_' . self::SPS_COUNTER_ACTION_NAME, [ $this, 'up_counter' ] );
		add_action( 'wp_ajax_' . self::SPS_COUNTER_ACTION_NAME, [ $this, 'up_counter' ] );
	}

	/**
	 * Add scripts and style.
	 *
	 * @return void
	 */
	public function add_scripts(): void {
		$url = get_template_directory_uri() . '/assets/';

		wp_enqueue_script( 'sps_build', $url . 'js/build.js', [ 'jquery' ], self::SPS_VERSION, true );
		wp_enqueue_script( 'sps_main', $url . 'js/main.js', [ 'jquery' ], self::SPS_VERSION, true );
		wp_enqueue_script( 'sps_html5shiv', '//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js', null, '3.7.0', false );
		wp_enqueue_script( 'sps_respond', '//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', null, '3.7.0', false );
		wp_enqueue_style( 'sps_main', $url . 'css/main.css', null, self::SPS_VERSION );
		wp_enqueue_style( 'sps_style', get_template_directory_uri() . '/style.css', null, self::SPS_VERSION );

		wp_localize_script(
			'sps_main',
			'SpsObject',
			[
				'url'       => admin_url( 'admin-ajax.php' ),
				'action'    => self::SPS_COUNTER_ACTION_NAME,
				'nonceCode' => wp_create_nonce( self::SPS_COUNTER_ACTION_NAME ),
			]
		);

		wp_script_add_data( 'sps_html5shiv', 'conditional', 'lt IE 9' );
		wp_script_add_data( 'sps_respond', 'conditional', 'lt IE 9' );
	}

	/**
	 * Theme support.
	 *
	 * @return void
	 */
	public function add_theme_support(): void {
		add_theme_support( 'custom-logo' );
	}

	/**
	 * Add SVG and Webp formats to upload.
	 *
	 * @param array $mimes Mimes type.
	 *
	 * @return array
	 */
	public function add_support_mimes( array $mimes ): array {

		$mimes['webp'] = 'image/webp';
		$mimes['svg']  = 'image/svg+xml';

		return $mimes;
	}

	/**
	 * Change logo css class.
	 *
	 * @param string $html Html.
	 *
	 * @return string
	 */
	public function change_logo_class( string $html ): string {
		$html = str_replace( 'custom-logo-link', 'logo', $html );

		return $html;
	}

	/**
	 * Add dashboard widget.
	 *
	 * @return void
	 */
	public function add_dashboard_widgets(): void {
		wp_add_dashboard_widget(
			'sps_statistic_quiz',
			__( 'Количество пройденного теста до конца.', 'sp-santa' ),
			[
				$this,
				'output_widget',
			]
		);
	}

	/**
	 * Output statistic quiz widget.
	 *
	 * @return void
	 */
	public function output_widget(): void {
		?>
		<div>
			<?php
			echo sprintf(
				'<p><b>%s:</b> %d</p>',
				esc_html( __( 'Количество пройденых тестов', 'sp-santa' ) ),
				esc_html( get_option( self::SPS_OPTIONS_COUNT_NAME ) ?? 0 )
			)
			?>
		</div>
		<?php
	}

	/**
	 * Up counter.
	 *
	 * @return void
	 */
	public function up_counter(): void {
		$nonce = isset( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : null;

		if ( empty( $nonce ) && ! wp_verify_nonce( $nonce, self::SPS_COUNTER_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Не верный одноразовый код', 'sp-santa' ) ] );
		}

		$count = get_option( self::SPS_OPTIONS_COUNT_NAME, null );

		if ( ! empty( $count ) ) {
			update_option( self::SPS_OPTIONS_COUNT_NAME, (int) $count + 1, false );
			wp_send_json_success();
		}

		add_option( self::SPS_OPTIONS_COUNT_NAME, 1, '', false );
		wp_send_json_success();
	}
}
