<?php
/**
 * Create Carbone settings.
 *
 * @package iwpdev/sp-santa
 */

namespace Iwpdev\SpSanta;

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * CarboneFields class file.
 */
class CarboneFields {
	/**
	 * CarboneFields construct.
	 */
	public function __construct() {

		$this->init();
	}

	/**
	 * Init all hooks.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'after_setup_theme', [ $this, 'carbon_fields_load' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_carbon_fields_main_page' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_carbon_fields_quiz' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_carbon_fields_finish' ] );
	}

	/**
	 * Load carbon field.
	 *
	 * @return void
	 */
	public function carbon_fields_load(): void {
		Carbon_Fields::boot();
	}

	/**
	 * Add carbone fields to main page.
	 *
	 * @return void
	 */
	public function add_carbon_fields_main_page(): void {
		Container::make( 'post_meta', __( 'Настройки главной страницы', 'sp-santa' ) )
			->where( 'post_id', 'IN', [ 5, 13, ] )
			->add_fields(
				[
					Field::make( 'text', 'sps_title', __( 'Заголовок', 'sp-santa' ) ),
					Field::make( 'text', 'sps_sub_title', __( 'Подзаголовок', 'sp-santa' ) ),
					Field::make( 'text', 'sps_button_ru_text', __( 'Текст кнопки ру.', 'sp-santa' ) )
						->set_width( 50 ),
					Field::make( 'text', 'sps_button_ru_link', __( 'Ссылка кнопки ру.', 'sp-santa' ) )
						->set_width( 50 ),
					Field::make( 'text', 'sps_button_uz_text', __( 'Текст кнопки uz.', 'sp-santa' ) )
						->set_width( 50 ),
					Field::make( 'text', 'sps_button_uz_link', __( 'Ссылка кнопки uz.', 'sp-santa' ) )
						->set_width( 50 ),

				]
			);
	}

	/**
	 * Add carbone fields to quiz page.
	 *
	 * @return void
	 */
	public function add_carbon_fields_quiz(): void {
		Container::make( 'post_meta', __( 'Настройки опроса', 'sp-santa' ) )
			->where( 'post_id', 'IN', [ 2, 18 ] )
			->add_fields(
				[
					Field::make( 'text', 'sps_button_text_start_quiz', __( 'Текст кнопки старт опроса', 'sp-santa' ) )
						->set_width( 25 ),
					Field::make( 'separator', 'sps_separator_questions', __( 'Вопросы' ) ),
					Field::make( 'complex', 'sps_quiz_questions', __( 'Вопросы опроса', 'sp-santa' ) )
						->add_fields(
							'sps_questions',
							__( 'Вопросы', 'sp-santa' ),
							[
								Field::make( 'image', 'image', __( 'Картинка', 'sp-santa' ) ),
								Field::make( 'text', 'title', __( 'Заголовок', 'sp-santa' ) ),
								Field::make( 'complex', 'questions', __( 'Вопросы', 'sp-santa' ) )
									->set_layout( 'tabbed-horizontal' )
									->add_fields(
										'questions',
										__( 'Вопрос', 'sp-santa' ),
										[
											Field::make( 'textarea', 'text', __( 'Заголовок', 'sp-santa' ) ),
											Field::make( 'select', 'question', __( 'Тип санты', 'sp-santa' ) )
												->set_options(
													[
														'elf'        => __( 'Эльф', 'sp-santa' ),
														'strategist' => __( 'Санта стратег', 'sp-santa' ),
														'fireman'    => __( 'Санта-пожарник', 'sp-santa' ),
														'grinch'     => __( 'Гринч', 'sp-santa' ),
													]
												),
										]
									),
							]
						),
				]
			);
	}

	/**
	 * Add carbone fields to finish steps.
	 *
	 * @return void
	 */
	public function add_carbon_fields_finish(): void {
		Container::make( 'post_meta', __( 'Настройки опроса', 'sp-santa' ) )
			->where( 'post_template', '=', 'finish.php' )
			->add_fields(
				[
					Field::make( 'text', 'sps_button_text_return_quiz', __( 'Текст кнопки пройти еще', 'sp-santa' ) )
						->set_width( 25 ),
					Field::make( 'text', 'sps_button_text_to_shop', __( 'Текст кнопки в  магазин', 'sp-santa' ) )
						->set_width( 25 ),
					Field::make( 'text', 'sps_button_link_to_shop', __( 'Cсылка на магазин', 'sp-santa' ) )
						->set_width( 25 ),
					Field::make( 'text', 'sps_button_shear_button', __( 'Текст поделится', 'sp-santa' ) )
						->set_width( 25 ),
					Field::make( 'separator', 'sps_separator', __( 'Ответы на Вопросы' ) ),
					Field::make( 'text', 'sps_answer_title', __( 'Заголовок', 'sp-santa' ) ),
					Field::make( 'textarea', 'sps_answer', __( 'Ответ', 'sp-santa' ) ),
					Field::make( 'image', 'sps_background', __( 'Задний фон' ) ),
					Field::make( 'image', 'sps_background_mobile', __( 'Задний фон мобильная' ) ),
				]
			);
	}
}
