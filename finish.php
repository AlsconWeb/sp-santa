<?php
/**
 * Template Name: Finish
 *
 * @package iwpdev/sp-santa
 */

if ( ! isset( $_COOKIE['sps_view'] ) ) {
	global $post;
	wp_redirect( get_permalink( $post->post_parent ), 301 );
	die();
}
get_header();

?>
<section>
	<div class="container">
		<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();

				$bg     = wp_get_attachment_image_url( carbon_get_the_post_meta( 'sps_background' ), 'full' );
				$bg_mob = wp_get_attachment_image_url( carbon_get_the_post_meta( 'sps_background_mobile' ), 'full' );
				global $post;
				?>
				<div
						class="finish active"
						style="--bg: url('<?php echo esc_url( $bg ); ?>'); --bg-mob: url('<?php echo esc_url( $bg_mob ); ?>');">
					<p class="button">
						<?php echo esc_html( carbon_get_the_post_meta( 'sps_answer_title' ) ?? '' ); ?>
					</p>
					<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'sps_answer' ) ?? '' ) ); ?>
					<div class="sheared-link">
						<p><?php echo esc_html( carbon_get_the_post_meta( 'sps_button_shear_button' ) ); ?></p>
						<?php echo do_shortcode( '[spbsm-share-buttons]' ); ?>
					</div>
					<div class="links">
						<a class="link icon-back" href="<?php echo esc_url( get_permalink( $post->post_parent ) ); ?>">
							<?php echo esc_html( carbon_get_the_post_meta( 'sps_button_text_return_quiz' ) ?? '' ); ?>
						</a>
						<a
								class="link icon-earth"
								href="<?php echo esc_url( carbon_get_the_post_meta( 'sps_button_link_to_shop' ) ?? '#' ); ?>">
							<?php echo esc_html( carbon_get_the_post_meta( 'sps_button_text_to_shop' ) ?? '' ); ?>
						</a>
					</div>
				</div>
				<?php
			}
		}
		?>
	</div>
</section>
