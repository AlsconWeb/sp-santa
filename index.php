<?php
/**
 * Index theme file.
 *
 * @package iwpdev/sp-santa
 */

get_header();
?>
	<section>
		<div class="container">
			<?php
			if ( have_posts() ) {
				while ( have_posts() ) {
					the_post();
					?>
					<h1 class="title"><?php echo esc_html( carbon_get_the_post_meta( 'sps_title' ) ?? '' ); ?></h1>
					<h3 class="title"><?php echo esc_html( carbon_get_the_post_meta( 'sps_sub_title' ) ?? '' ); ?></h3>
					<div class="buttons">
						<a
								class="button"
								href="<?php echo esc_url( carbon_get_the_post_meta( 'sps_button_ru_link' ) ?? '#' ); ?>">
							<?php echo esc_html( carbon_get_the_post_meta( 'sps_button_ru_text' ) ?? '' ); ?>
						</a>
						<a
								class="button"
								href="<?php echo esc_url( carbon_get_the_post_meta( 'sps_button_uz_link' ) ?? '#' ); ?>">
							<?php echo esc_html( carbon_get_the_post_meta( 'sps_button_uz_text' ) ?? '' ); ?>
						</a>
					</div>
					<?php
				}
			}
			?>
		</div>
	</section>
<?php

get_footer();

