/* global SpsObject */

/**
 * @param SpsObject.url
 * @param SpsObject.action
 * @param SpsObject.nonceCode
 */

jQuery( document ).ready( function( $ ) {
	let elf = [];
	let strategist = [];
	let fireman = [];
	let grinch = [];

	let countQuestion = $( '.question' ).length;
	let i = 0;


	$( '.radio-button input[type=radio]' ).click( function( e ) {

		let value = $( this ).data( 'type' );

		switch ( value ) {
			case 'elf':
				elf.push( value );
				break;
			case 'strategist':
				strategist.push( value );
				break;
			case 'fireman':
				fireman.push( value );
				break;
			case 'grinch':
				grinch.push( value );
				break;
		}

		i++;

		$( this ).parents( '.question' ).removeClass( 'active' );
		if ( i < countQuestion ) {
			$( this ).parents( '.question' ).next().addClass( 'active' );
		}

		if ( i >= countQuestion ) {

			let all = [];
			all.push( elf, strategist, fireman, grinch );

			let answer = all.reduce( function( maxI, el, i, arr ) {
				return el.length > arr[ maxI ].length ? i : maxI;
			}, 0 )

			sendAjaxCounter();

			switch ( all[ answer ][ 0 ] ) {
				case 'elf':
					setCookie( 'sps_view', true, 1 );
					location.href = window.location.pathname + '/elf';
					break;
				case 'strategist':
					setCookie( 'sps_view', true, 1 );
					location.href = window.location.pathname + '/strategist';
					break;
				case 'fireman':
					setCookie( 'sps_view', true, 1 );
					location.href = window.location.pathname + '/fireman';
					break;
				case 'grinch':
					setCookie( 'sps_view', true, 1 );
					location.href = window.location.pathname + '/grinch';
					break;
			}

			if ( $( window ).height() <= 479 ) {
				$( 'header' ).show();
				$( 'body' ).removeClass( 'no-header' );
			}
		}

		$( '.link.icon-back' ).click( function( e ) {
			e.preventDefault()

			elf = [];
			strategist = [];
			fireman = []
			grinch = [];
			i = 0
			$( '#quiz' ).trigger( 'reset' );

		} )
	} );

	/**
	 * Set cookie.
	 *
	 * @param name cookie name.
	 * @param value cookie value.
	 * @param days cookie date expires.
	 */
	function setCookie( name, value, days ) {
		var expires = '';
		if ( days ) {
			var date = new Date();
			date.setTime( date.getTime() + ( days * 24 * 60 * 60 * 1000 ) );
			expires = '; expires=' + date.toUTCString();
		}
		document.cookie = name + '=' + ( value || '' ) + expires + '; path=/';
	}

	/**
	 * Send Ajax count.
	 */
	function sendAjaxCounter() {
		let data = {
			action: SpsObject.action,
			nonce: SpsObject.nonceCode
		}

		$.ajax( {
			type: 'POST',
			url: SpsObject.url,
			data: data,
			cache: false,
			success: function( res ) {
				console.log( res )

			},
			error: function( xhr, ajaxOptions, thrownError ) {
				console.log( 'error...', xhr );
				//error logging
			}
		} );
	}
} );